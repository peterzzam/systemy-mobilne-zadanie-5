package pl.edu.pb.sensorapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class SensorActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SensorManager sensorManager;
    private List<Sensor> sensorList;
    private SensorAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SensorActivity.this, RecyclerView.VERTICAL, false);
        recyclerView = findViewById(R.id.sensor_recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);

        if(adapter == null) {
            adapter = new SensorAdapter(sensorList, SensorActivity.this);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.fragment_task_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.sensor_count:
                showSensorCount();
                break;
        }
        return true;
    }

    public void showSensorCount() {
        String subtitle = getString(R.string.sensors_count, sensorList.size());
        AppCompatActivity appCompatActivity = (AppCompatActivity) this;
        appCompatActivity.getSupportActionBar().setSubtitle(subtitle);
    }

    public class SensorAdapter extends RecyclerView.Adapter<SensorAdapter.SensorHolder> {

        private SensorManager sensorManager;
        private List<Sensor> sensorList;
        private RecyclerView recyclerView;
        private SensorAdapter adapter;
        private Context context;

        public SensorAdapter(List<Sensor> sensorList, Context context) {
            this.sensorList = sensorList;
            this.context = context;
        }

        @NonNull
        @Override
        public SensorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sensor_list_item, parent, false);
            return new SensorHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SensorHolder holder, int position) {
            Sensor sensor = sensorList.get(position);
            holder.sensorNameTextView.setText(sensor.getName());
            Log.d(null, sensor.getName() + ", vendor: " + sensor.getVendor() + ", maximum range: " + sensor.getMaximumRange());
            switch(sensor.getType()) {
                case Sensor.TYPE_LIGHT:
                    holder.sensorNameTextView.setTypeface(Typeface.DEFAULT_BOLD);
                    holder.sensorNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(view.getContext(), SensorDetailsActivity.class);
                            view.getContext().startActivity(intent);
                        }
                    });
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    holder.sensorNameTextView.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                    holder.sensorNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(view.getContext(), LocationActivity.class);
                            view.getContext().startActivity(intent);
                        }
                    });
            }
        }

        @Override
        public int getItemCount() {
            return sensorList.size();
        }

        public class SensorHolder extends RecyclerView.ViewHolder {


            public ImageView sensorIconImageView;
            public TextView sensorNameTextView;
            public Sensor sensor;

            public SensorHolder(@NonNull View itemView) {
                super(itemView);
                sensorIconImageView = (ImageView) itemView.findViewById(R.id.sensor_icon);
                sensorNameTextView = (TextView) itemView.findViewById(R.id.sensor_name);

            }

            public void bind(Sensor sensor) {
                this.sensor = sensor;
            }
        }
    }


}